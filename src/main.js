// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import Vuelidate from 'vuelidate'
import Toasted from 'vue-toasted';
import VueTheMask from 'vue-the-mask'
import Datetime from 'vue-datetime'
import App from './App'
import router from './router'
import store from './store'

import axios from 'axios'
import config from './env'


Vue.use(Datetime)
Vue.use(VueTheMask)
Vue.use(Toasted)
Vue.use(Vuelidate)
Vue.use(require('vue-moment'));

Vue.directive('focus', {
  inserted: function (el) {
      el.focus()
  }
})

axios.defaults.baseURL = config.ROOT_API
axios.defaults.headers.common['Content-Type'] = 'application/json'



Vue.config.productionTip = false
Vue.prototype.$axios = axios
Vue.prototype.$NODE_ENV = config.NODE_ENV






/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  router,
  components: { App },
  template: '<App/>'
})