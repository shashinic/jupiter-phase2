import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import Toasted from 'vue-toasted';
Vue.use(Toasted)
import createPersistedState from 'vuex-persistedstate'
Vue.use(Vuex)
// Vue.use(Toasted)

export default new Vuex.Store({
  state: {
    platformName: "",
    merchantId: "",
    token: "",
    isUserNew: null,
    username: "",
    smsCode: "",
    stepProgress: {
       step1: "progress",
       step2: "",
       step3: "",
       step4: ""
    },
    flowCompleted: {merchantId: "", status: false},
    flowStatus: null, //approved or not-approved
  },
  mutations: {
    SET_ITEM: function (state,item,value) {
      console.log("SET_ITEM:" + item + "    ---   " + value)
      this.state['item']=value
    },
    setStepProgress: function (state,{step,status}){
      console.log('step is  ',step);
      let nextStep = step+1;
       if(step == 'default'){
         Object.assign(this.state.stepProgress,{ step1: "progress",step2: "", step3: "",step4: ""} )
       }
       else if(state.stepProgress['step'+step] != 'completed'){
             this.state.stepProgress['step'+step] = status;
             if(nextStep <= 4 && status == 'completed'){
               this.state.stepProgress['step'+nextStep] = 'progress';
             }
       }
    },
    setFlowStatus: function (state,{merchantId,flowCompleted,flowStatus}){
         this.state.flowCompleted = {merchantId: state.merchantId, status: flowCompleted};
         this.state.flowStatus = flowStatus;
    },
    logError: function (state,data){
       Vue.toasted.show('Server side error, please contact the support team.',{
          type : 'error',
          duration : 3000,
          position: "top-right", 
        })
      axios({
        "url": "/logging/error",
        "method": "POST",
        "headers": { "content-type": "application/json"},
        "data": JSON.stringify(data)
      })
      .then(response => {
        console.log('/logging/error response:',response);
      })
    }
  },
  actions: {
    
  },
  plugins: [createPersistedState({
    paths: ['stepProgress','flowCompleted','flowStatus','merchantId']
 })]

})