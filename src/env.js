'use strict'
module.exports = {
  NODE_ENV: 'development',
  ROOT_API: 'http://stage-api.jupiterhq.com/api/v1'
  // ROOT_API: 'http://localhost:4040/api/v1'
}
